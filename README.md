# StarAI

Una Inteligencia Artifical Simple que juega estrellita donde estas.

## Equipo
* Alejandro Salgado Gómez [asalgad2@eafit.edu.co](mailto:asalgad2@eafit.edu.co)
* Luis Miguel Mejía Suárez [lmejias3@eafit.edu.co](mailto:lmejias3@eafit.edu.co)

## Uso
Para hacer uso de StarAI en su programa, debe de copiar el archivo AgenteAlejandroLuis.py
a la carpeta donde se encuentra su programa.

Luego debe de importar el agente, instanciarlo y ejecutar el metodo _"play"_ continuamente.

A continuación un ejemplo

```python
from AgenteAlejandroLuis import AgenteAlejandroLuis
#El constructor del agente recibe como parametro cuál jugador es (1 o 2)
agente = AgenteAlejandroLuis(1)
#El método play recibe el id del jugador, el resultado de su ultima accion, la accion que realizo el oponente
#en el formato especificado en el enunciado y la posicion de su propia estrella.
jugada1 = agente.play(jugador, resultado, accion_oponente, estrellita)
```

## Algoritmos
StarAI juega usando un hidden Markov model para calcular las probabilidades de la posicion del rival durante todo el juego,
para determinar la siguiente accion a ejecutar se utiliza VPI para encontrar cual es la mejor opcion. Es importante
aclarar que VPI solo computa la mejor opcion entre disparar y sensar, para saber si debe moverse o no el agente simula a
su oponente y en caso de descubrir una amenaza factible en su contra se mueve para evitarla.

En este **[Vídeo](https://www.youtube.com/watch?v=lcdjadTzHTc&feature=youtu.be)** se da una explicación detallada de los algoritmos implementados.
