import random

class AgenteAlejandroLuis:
    #ClASS VARS
    ##Actions
    SHOOT = 1
    SENSE = 2
    MOVE  = 3

    ##Movements
    UP    = 1
    DOWN  = 3
    LEFT  = 4
    RIGHT = 2

    ##Results
    ACTION = 0
    PARAM  = 1
    RESULT = 2

    ##Transitions
    VPI = 0
    HMM = 1

    ##SHOT RESULTS
    MISS = 0
    HIT  = 1

                  # vpi  hmm
    transitions = [  {}, {} ]

    #P(col | pos)    g      y      o     r
    model = { 0 : [ 0.70,  0.15,  0.10, 0.05 ],
              1 : [ 0.17,  0.60,  0.17, 0.06 ],
              2 : [ 0.06,  0.17,  0.60, 0.17 ],
              3 : [ 0.05,  0.12,  0.23, 0.60 ],
              4 : [ 0.03,  0.07,  0.10, 0.80 ] }

    colorDic = { 0 : "verde",
                 1 : "amarillo",
                 2 : "anaranjado",
                 3 : "rojo",
                 "verde"  : 0,
                 "amarillo" : 1,
                 "anaranjado" : 2,
                 "rojo"    : 3 }

    def __init__(self, player):
        #agent data
        self.player = player
        self.turn = 1
        self.probs = [1/25 for i in range(25)]
        self.opp_probs = [1/25 for i in range(25)]
        self.colors = ["verde", "amarillo", "anaranjado", "rojo"]
        self.lastParam = None
        self.lastAction = None

        #agent parameters
        self.shoot_utility = 50
        self.threshold = 0.15
        self.danger = 0.40

        #init agent transition model
        self.initializeTransitions()

    def initializeTransitions(self):
        vpiTable = AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.VPI]
        hmmTable = AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.HMM]

        # Initialize tables with 0
        for i in range(5):
            for j in range(5):
                pos = (i,j)

                vpiTable[pos] = [0.0]*25
                hmmTable[pos] = [0.0]*25

        # Fill all the transition tables
        for i in range(5):
            for j in range(5):
                pos = (i,j)

                vpitable = vpiTable[pos]
                hmmtable = hmmTable[pos]

                # Vpi
                idx = self.fromPosToIdx(pos)
                vpitable[idx] = 0.8

                positions = self.getValidNewPositions(pos)
                size = len(positions) - 1

                for idx in positions[1:]:
                    vpitable[idx] = 0.2/size

                # Hmm
                if i != 0 and j != 0 and i != 4 and j != 4: # if is center
                    idx = self.fromPosToIdx(pos)
                    vpitable[idx] = 0.0
                    positions = self.getValidNewPositions(pos)
                    size = len(positions) - 1

                    for idx in positions[1:]:
                        hmmtable[idx] = 1/size
                else:
                    positions = self.getValidNewPositions(pos)
                    size = len(positions)

                    for idx in positions:
                        hmmtable[idx] = 1/size

    def getValidNewPositions(self, pos):
        i,j = pos

        positions = []

        idx = self.fromPosToIdx((i,j)) # center
        positions.append(idx)

        if i-1 >= 0:
            idx = self.fromPosToIdx((i-1,j)) # up
            positions.append(idx)
        if j+1 <= 4:
            idx = self.fromPosToIdx((i,j+1)) # right
            positions.append(idx)
        if i+1 <= 4:
            idx = self.fromPosToIdx((i+1,j)) # down
            positions.append(idx)
        if j-1 >= 0:
            idx = self.fromPosToIdx((i,j-1)) # left
            positions.append(idx)

        return positions

    #update the beliefs table given a hit shot
    def hit_shot(self, beliefs, pos):
        for index in range(25):
            beliefs[index] = 0.0
        beliefs[pos] = 1.0
        return beliefs

    #update the beliefs table given a missed shot
    #return the distribution to an uniform state,
    #but remember where the probability was high to sense there
    def miss_shot(self, beliefs, pos):
        argmax = None
        max_v = 0
        beliefs[pos] = 0.0
        for index in range(25):
            v = beliefs[index]
            if v > max_v:
                argmax = index
                max_v = v
        return argmax, [1/25 for i in range(25)]

    #choose a random valid position to move the star
    def escape(self, estrellita):
        moves = []
        if estrellita > 4:
            moves.append(AgenteAlejandroLuis.UP)
        if estrellita < 20:
            moves.append(AgenteAlejandroLuis.DOWN)
        if estrellita % 5 != 0:
            moves.append(AgenteAlejandroLuis.LEFT)
        if estrellita % 5 != 4:
            moves.append(AgenteAlejandroLuis.RIGHT)
        move = random.choice(moves)
        return AgenteAlejandroLuis.MOVE, move

    #Shoot randomly between all positions with the maximum probability
    def shoot(self, beliefs, best):
        shots = []
        for index in range(25):
            if beliefs[index] == best:
                shots.append(index)
        shot = random.choice(shots)
        return AgenteAlejandroLuis.SHOOT, shot

    #Main method of the agent
    #All code logic runs here
    def play(self, _, result, opponent, estrellita):
        #estrellita -= 1

        #check self last shot
        if self.lastAction == AgenteAlejandroLuis.SHOOT:
            if result == AgenteAlejandroLuis.HIT:
                self.probs = self.hit_shot(self.probs, self.lastParam)
            else: #result == AgenteAlejandroLuis.MISS
                sense_pos, self.probs = self.miss_shot(self.probs, self.lastParam)
                act, par = AgenteAlejandroLuis.SENSE, sense_pos
                self.lastAction = act
                self.lastParam = par
                self.turn += 1
                return act, par+1

        #check opponent last shot
        if opponent[AgenteAlejandroLuis.ACTION] == AgenteAlejandroLuis.SHOOT:
            if opponent[AgenteAlejandroLuis.RESULT] == AgenteAlejandroLuis.HIT:
                self.opp_probs = self.hit_shot(self.opp_probs, opponent[AgenteAlejandroLuis.PARAM]-1)

        #self hmm
        if self.lastAction == AgenteAlejandroLuis.SENSE and opponent[AgenteAlejandroLuis.ACTION] == AgenteAlejandroLuis.MOVE:
            self.probs = self.analyzeEvidence(self.lastParam, result, self.probs)
            self.probs = self.calcForwardProb(self.probs, AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.HMM])

        elif opponent[AgenteAlejandroLuis.ACTION] == AgenteAlejandroLuis.MOVE:
            self.probs = self.calcForwardProb(self.probs, AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.HMM])

        elif self.lastAction == AgenteAlejandroLuis.SENSE:
            self.probs = self.analyzeEvidence(self.lastParam, result, self.probs)

        #opponent hmm
        if opponent[AgenteAlejandroLuis.ACTION] == AgenteAlejandroLuis.SENSE:
            self.opp_probs = self.analyzeEvidence(opponent[AgenteAlejandroLuis.PARAM]-1, opponent[AgenteAlejandroLuis.RESULT], self.opp_probs)

        #behave differently in the last turn
        if self.turn == 30:
            #if in danger, the best thing to do is move
            if self.player == 1 and self.opp_probs[estrellita] >= self.danger:
                return self.escape(estrellita)

            #if not in danger, shoot
            _, best = self.meu(self.probs)
            act, par = self.shoot(self.probs, self.probs[best])
            return act, par+1

        #if is not the last turn
        #if in danger, move
        if self.opp_probs[estrellita] >= self.danger:
            self.opp_probs = self.calcForwardProb(self.opp_probs, AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.HMM])
            act, par = self.escape(estrellita)
            self.lastParam = par
        else: #if not, choose between shoot and sense using vpi
            act, par = self.getAction()
            self.lastParam = par
            par += 1

        #end turn
        self.lastAction = act
        self.turn += 1
        return act, par

    def calcForwardProb(self, probs, transition):
        newProbs = []

        for i in range(5):
            for j in range(5):
                xt = (i,j)
                result = 0
                for k in range(5):
                    for l in range(5):
                        xt_1 = (k,l)

                        idx = self.fromPosToIdx(xt_1)
                        pxt_1 = probs[idx]

                        table = transition[xt_1]

                        idx = self.fromPosToIdx(xt)
                        pxt = table[idx]

                        result += pxt * pxt_1
                newProbs.append(result)

        return newProbs

    def analyzeEvidence(self, pos, color, probs):
        e = self.fromIdxToPos(pos)
        newDist = []

        for i in range(5):
            for j in range(5):
                xt = (i,j)

                dist = self.calcDist(e, xt)

                table = AgenteAlejandroLuis.model[dist]
                colorIdx = AgenteAlejandroLuis.colorDic[color]
                pex = table[colorIdx]

                posIdx = self.fromPosToIdx(xt)
                px = probs[posIdx]

                newDist.append( pex * px )

        return self.normalize(newDist)

    def calcDist(self, pos1, pos2):
        i1,j1 = pos1
        i2,j2 = pos2

        dist =  abs(i1-i2) + abs(j1-j2)

        if dist > 4:
            return 4
        else:
            return dist

    def normalize(self, dist):
        s = sum(dist)

        #avoid normalize something that is already normalized
        if s == 0 or s == 1:
          return dist

        size = len(dist)

        for i in range(size):
            dist[i] /= s

        return dist

    def fromPosToIdx(self, pos):
        i,j = pos
        return j + i*5

    def fromIdxToPos(self, idx):
        i = idx // 5
        j = idx % 5
        return (i,j)

    def meu(self, dist):
        maxVal = 0
        maxPos = 0
        for i in range(25):
            val = dist[i] * self.shoot_utility
            if val > maxVal:
                maxVal = val
                maxPos = i

        return maxVal, maxPos

    def getAction(self):
        maxVpi = 0
        maxVpiPos = 0

        meu_0, maxPos = self.meu(self.probs)

        for pos in range(25):
            currentVpi = self.vpi(pos, meu_0)

            if currentVpi > maxVpi:
                maxVpi = currentVpi
                maxVpiPos = pos

        thrshld = ((meu_0 + maxVpi) / meu_0) -1
        if thrshld > self.threshold:
            return AgenteAlejandroLuis.SENSE, maxVpiPos
        else:
            return self.shoot(self.probs, self.probs[maxPos])

    def vpi(self, pos, meu_0):
        result = 0
        for color in self.colors:
            for move in range(25):
                pem = self.calcJoinProb(pos, color, move)

                bxt = self.analyzeEvidence(pos, color, self.probs)
                bxt_1 = self.calcForwardProb(bxt, AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.VPI])

                meu_em, _ = self.meu(bxt_1)

                result += pem * meu_em

        return result - meu_0

    def calcJoinProb(self, pos, color, move):
        pos = self.fromIdxToPos(pos)
        result = 0

        for xt in range(25):
            bxt = self.probs[xt]

            xtPos = self.fromIdxToPos(xt)
            dist = self.calcDist(xtPos, pos)
            table = AgenteAlejandroLuis.model[dist]
            colorIdx = AgenteAlejandroLuis.colorDic[color]
            pext = table[colorIdx]

            pmxt = AgenteAlejandroLuis.transitions[AgenteAlejandroLuis.VPI][xtPos][move]

            result += bxt * pext * pmxt

        return result
